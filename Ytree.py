#!/usr/bin/env python

import sys, os
import numpy as np
import vcf
import operator
import re
import gzip
import argparse


HG19_Y_LEN = 59373566
HG38_Y_LEN = 57227415


def get_args():
    parser = argparse.ArgumentParser(description="Bayesian method to assign Y "\
                                     "haplogroups from variant calls")
    parser.add_argument("--min_post_pr", action="store", type=float, default=0.8, 
                        help="The minimum posterior probability required to call"\
                        " a Y haplogroup (default: 0.8)")
    parser.add_argument("--err_prob", action="store", type=float, default=0.01,
                        help="The probability that a variant call is an error "\
                        "(default: 0.01)")
    parser.add_argument("--assembly", required=True, choices=["hg19", "hg38"], 
                        help="Assembly version")
    parser.add_argument("--bed_file", action="store", default=None, 
                        help="File of targeted regions in bed format (no file "\
                        "means all bases were targeted, i.e. WGS)")
    parser.add_argument("tree_file", help="File containing the "\
                        "tree structure of Y haplogroups "\
                        "(e.g. isogg_2016_tree.txt)")
    parser.add_argument("snp_file", help="File containing information "\
                        "about Y haplogroup defining SNPs "\
                        "(e.g. y_snp_info.txt)") 
    parser.add_argument("vcf_file", help="VCF file to call Y haplogroups from")
    args = parser.parse_args()
    return args
    

def open_file(filename):
    if filename.endswith(".gz"):
        f = gzip.open(filename, "rb")
    else:
        f = open(filename, "r")

    return f


class TreeNode(object):
    def __init__(self):
        self.name = None
        self.aliases = []
        self.alt_names = []
        self.children = []
        self.parent_name = None
        self.parent = None
        self.snps = []
        

    def is_root(self):
        if self.parent is None:
            return True
        else:
            return False


    def is_leaf(self):
        if len(self.children) > 0:
            return False
        else:
            return True


    def get_all_leaves(self):
        '''For any particular node, get all the leaves that are descended
        from it'''
        if self.is_leaf():
            return [self]
        else:
            # this is not a leaf, get leaves from children
            leaves = []
            for child in self.children:
                leaves.extend(child.get_all_leaves())
            return leaves


    def assign_snp_gtypes(self):
        '''Pushes SNP assignments down the tree'''
        if self.is_root():
            # get all snps that are in tree
            all_snps = self.get_all_snps()

            # give each snp an index into gtype array
            i = 0
            for snp in all_snps:
                snp.gtype_idx = i
                i += 1

            # root has ancestral genotype for every SNP
            self.gtype_array = np.zeros(len(all_snps), dtype=np.int8)

        else:
            # copy gtype array from parent
            self.gtype_array = np.array(self.parent.gtype_array)

            # if this node is associated with a SNP
            # then it, and its children have the derived genotype
            for snp in self.snps:
                self.gtype_array[snp.gtype_idx] = 1

        # recursively call children
        for child in self.children:
            child.assign_snp_gtypes()


    def get_all_snps(self):
        '''Get the list of snp objects from the entire tree'''
        if self.is_leaf():
            # return a copy of the list of SNPs
            # for this leaf
            return list(self.snps)
        else:
            # copy our own SNPs
            all_snps = list(self.snps)

            # add SNPs from children
            for child in self.children:
                all_snps.extend(child.get_all_snps())

        return all_snps


    def sum_post_prs(self, post_pr_dict):
        '''Take a dictionary of posterior probabilities for each leaf
        and sum them to get posterior probabilities for internal branches'''
        if self.is_leaf():
            pass
        else:
            post_pr_sum = 0
            for child in self.children:
                child.sum_post_prs(post_pr_dict)
                child_post_pr = post_pr_dict[child.name]
                post_pr_sum += child_post_pr
            post_pr_dict[self.name] = post_pr_sum


    def get_highest_post_pr(self, post_pr_dict, min_post_pr):
        '''Find the branch (internal or leaf) with the lowest
        posterior probability above a minimum threshold'''
        if self.is_leaf():
            return self
        else:
            # find the child with the highest posterior probability
            max_child = None
            max_post_pr_child = 0.0
            for child in self.children:
                child_post_pr = post_pr_dict[child.name]

                if child_post_pr > max_post_pr_child:
                    max_child = child
                    max_post_pr_child = child_post_pr

                    # print max_child.name + ":" + str(max_post_pr_child)

            if max_post_pr_child < min_post_pr:
                return self
            else:
                return max_child.get_highest_post_pr(post_pr_dict, min_post_pr)


class SNP(object):
    def __init__(self):
        self.id = None
        self.start = None
        self.ancestral = None
        self.derived = None
        self.haplogroup = None
        self.gtype_idx = None

    def __str__(self):
        return "\t".join([self.id, str(self.start), self.haplogroup])

  
def parse_snps(mut_path, assembly):
    '''Take the snp info file and create a dictionary keyed
    on SNP id with a SNP object as values'''
  
    f = open(mut_path, "r")
    header = f.readline()
  
    snp_dict = {}
    haplogroup_dict = {}
    for line in f:
    	words = line.rstrip().split("\t")
        snp = SNP()
      
        snp.id = words[0]
        snp.haplogroup = words[1]
        snp.ancestral = words[4]
        snp.derived = words[5]
        snp.ref_base = words[6]
        snp.alt_names = words[7].split(",")
      
        if assembly == "hg38":
            snp.start = int(words[3])
        elif assembly == "hg19":
            snp.start = int(words[2])
        else:
            raise ValueError("unknown assembly name %s" % assembly)
        
        snp_dict[snp.id] = snp

        if snp.haplogroup in haplogroup_dict:
            haplogroup_dict[snp.haplogroup].append(snp)
        else:
            haplogroup_dict[snp.haplogroup] = [snp]
      
    f.close()
    return snp_dict, haplogroup_dict
        

def parse_tree(snp_dict, tree_path, haplogroup_dict):
    '''Takes dictionary of SNPs (and their associated haplotypes) 
    as well as a file containing the tree backbone with non-canonical
    tree haplotypes.
    '''
    f = open(tree_path, "r")
    
    node_dict = {}
    
    for line in f:
      	words = line.rstrip().split("\t")
      	tree_node = TreeNode()
        tree_node.name = words[0]
        
        if tree_node.name in node_dict:
          	raise ValueError("Same node seen multiple times: %s\n" % tree_node.name)
						
        if words[1] == '-':
            # this should be root
            if words[0] != "Root":
                raise ValueError("Expected node without parent to be named 'Root'")
            tree_node.parent_name = None
        else:
            tree_node.parent_name = words[1]
            
        node_dict[tree_node.name] = tree_node
    
    # get all unique haplogroup names from the SNPs, use these 
    # to build trees, checking each node for whether it has already 
    # been defined (either by other SNP or by non-canonical backbone)
    haplogroups = set([snp.haplogroup for snp in snp_dict.values()])
    
    for hgroup in haplogroups:
      	if hgroup in node_dict:
            # already seen this haplogroup...
            tree_node = node_dict[hgroup]
        else:
            # create new node for this haplogroup
            tree_node = TreeNode()
            tree_node.name = hgroup
            node_dict[hgroup] = tree_node
            
            # connect this tree node to parents, using canonical 
            # naming system, until we hit an already known parent
            parent_name = hgroup[:-1]
            tree_node.parent_name = parent_name
            while len(parent_name) > 0 and parent_name not in node_dict:
                # create a node for this parent
                parent = TreeNode()
                parent.name = parent_name
                node_dict[parent_name] = parent
                
                # move on to next parent
                parent_name = parent_name[:-1]
                parent.parent_name = parent_name
        
        # add this SNP to this tree node
        for snp in haplogroup_dict[hgroup]:
            tree_node.snps.append(snp)
        
    root = build_tree(node_dict)
    return root
  
      	
def build_tree(node_dict):
    '''Returns the root node of a tree from a dictionary of tree
    nodes'''
    for node in node_dict.values():
        # get parent of this node
        if node.parent_name is None:
            # this is the root
            pass
        else:
            if node.parent is None:
                # first time we're setting this node's parent
                parent_node = node_dict[node.parent_name]
                parent_node.children.append(node)
                node.parent = parent_node
            else:
                # can see same node multiple times in dict
                # because there are multiple aliases for the same node
                pass

    return node_dict["Root"]


def parse_vcf(snp_dict, n_snps, vcf_path, target_array):
    '''Returns a dictionary of genotypes keyed on the individual id. Values are
    an array of genotypes in the same order as the above genotype array'''

    snp_base_dict = dict([(x.start, x) for x in snp_dict.values()])
    
    vcf_f = open_file(vcf_path)
    vcf_reader = vcf.VCFReader(vcf_f)

    # initalize a genotype array to missing data
    # set targeted regions to match the ref base which are not listed in vcf
    gtype_array = np.empty(n_snps, dtype=np.int8)
    gtype_array[:] = -1
    for snp in snp_dict.values():
        if target_array[snp.start-1] == 1:
            if snp.ref_base == snp.ancestral:
                gtype_array[snp.gtype_idx] = 0
            elif snp.ref_base == snp.derived:
                gtype_array[snp.gtype_idx] = 1

    gtype_dict = {}
    sample_names = vcf_reader.sample_names
    for indv in sample_names:
        gtype_dict[indv] = gtype_array.copy()

    for vcf_row in vcf_reader:
        if (vcf_row.chrom_name != "Y") and (vcf_row.chrom_name != "chrY"):
            continue
        
        if vcf_row.start not in snp_base_dict:
            continue

        snp = snp_base_dict[vcf_row.start]
        idx = snp.gtype_idx

        gtypes = vcf_row.get_field_strs("GT")
        
        for i in range(len(sample_names)):
            indv_name = sample_names[i]
            indv_gtype = gtypes[i]

            if indv_gtype == "." or indv_gtype == "./." or \
              indv_gtype == "0/1" or indv_gtype == "1/0":
              # this is missing data or an incorrectly called het site
              gtype_dict[indv_name][idx] = -1
              
            elif indv_gtype == "0/0" or indv_gtype == "0":
                # this a match to the reference base - check if ancestral/derived
                if vcf_row.ref_base == snp.ancestral:
                    gtype_dict[indv_name][idx] = 0
                elif vcf_row.ref_base == snp.derived:
                    gtype_dict[indv_name][idx] = 1
  
            elif indv_gtype == "1/1" or indv_gtype == "1":
                # this is a match to the alt base - check if ancestral/derived
                if vcf_row.alt_base == snp.ancestral:
                    gtype_dict[indv_name][idx] = 0
                elif vcf_row.alt_base == snp.derived:
                    gtype_dict[indv_name][idx] = 1
                    

    vcf_f.close()
    return gtype_dict


def make_empty_y_array(assembly):
    '''Returns an array the length of Y chrom with all -1s for missing data'''

    if assembly == "hg19":
        a = np.empty(HG19_Y_LEN, dtype=np.int8)
    elif assembly == "hg38":
        a = np.empty(HG38_Y_LEN, dtype=np.int8)
    a[:] = -1

    return a


def parse_targets(bed_file, target_array):
    '''Updates Y chromosome array with targeted regions 
    indicated with a 1 and non-targeted regions indicated with a -1'''

    bed_f = open(bed_file, "r")
    for line in bed_f:
        words = line.rstrip().split("\t")
        chrom = words[0]
        start = int(words[1])
        end = int(words[2])

        if (chrom != "chrY") and (chrom != "Y"):
            continue

        target_array[start:end] = 1
        
    bed_f.close()
    return target_array


def calc_post_pr(leaves, gtype_indv_dict, err_prob, all_snps):

    indv_post_prs = {}
    for indv in gtype_indv_dict:
        sys.stderr.write(".")
        # make a dictionary to store the logL and postPr for each leaf
        logL_dict = {}
        post_pr_dict = {}

        for leaf in leaves:
            indv_gtype_array = gtype_indv_dict[indv]
            leaf_gtype_array = leaf.gtype_array

            gtype_matches = np.sum((indv_gtype_array != -1) &
                                   (indv_gtype_array == leaf_gtype_array))


            gtype_mismatches = np.sum((indv_gtype_array !=-1) &
                                      (indv_gtype_array != leaf_gtype_array))

            logL = ((np.log(1.0 - err_prob) * gtype_matches) +
                    (np.log(err_prob) * gtype_mismatches))

            logL_dict[leaf.name] = logL            

        logL_array = np.array(logL_dict.values())
        logL_sum = sumlogs(logL_array)

        for leaf in leaves:
            post_pr = np.exp(logL_dict[leaf.name] - logL_sum)
            post_pr_dict[leaf.name] = post_pr

        indv_post_prs[indv] = post_pr_dict

    sys.stderr.write("\n")
    return indv_post_prs


def sum_two_logs(lnx, lny):
    if lnx > lny:
        return lnx + np.log(1 + np.exp(lny - lnx))
    else:
        return lny + np.log(1 + np.exp(lnx - lny))



def sumlogs(lnx):
    if lnx.size == 2:
        return sum_two_logs(lnx[0], lnx[1])

    return sum_two_logs(lnx[0], sumlogs(lnx[1:]))


def main():
    args = get_args()
    tree_path = args.tree_file
    mut_path = args.snp_file
    vcf_path = args.vcf_file
    assembly = args.assembly
    bed_file = args.bed_file

    snp_dict, haplogroup_dict = parse_snps(mut_path, assembly)
    sys.stderr.write("%d SNPs\n" % len(snp_dict))

    sys.stderr.write("Building tree and assigning Y SNPs\n")
    tree_root = parse_tree(snp_dict, tree_path, haplogroup_dict)
    tree_root.assign_snp_gtypes()

    all_snps = tree_root.get_all_snps()
    n_snps = len(all_snps)

    # give each snp an index into gtype array
    i = 0
    for snp in all_snps:
        snp_dict[snp.id].gtype_idx = i
        i += 1

    sys.stderr.write("Parsing VCF file and generating genotype arrays for "
                     "each indvidual\n")

    # make array of regions that were targeted (whole chrom for WGS)
    target_array = make_empty_y_array(assembly)
    if bed_file is None:
        target_array[:] = 1
    else:
        target_array = parse_targets(bed_file, target_array)

    gtype_indv_dict = parse_vcf(snp_dict, n_snps, vcf_path, target_array)

    # check that there are enough SNPs to call Y haplogroups
    for indv in gtype_indv_dict:
        n_snps = sum(gtype_indv_dict[indv] != -1)
        if n_snps < 10:
           sys.stderr.write("WARNING: %s has only %d informative SNP/s\n" % (indv, n_snps))

    
    sys.stderr.write("Calculating log likelihoods across the Y tree leaves "
                     "for each individual\n")
    leaves = tree_root.get_all_leaves()
    indv_post_pr = calc_post_pr(leaves, gtype_indv_dict, args.err_prob, all_snps)

    sys.stdout.write("\t".join(["INDV", "MAJOR.CLADE", "Y.HAPLOGROUP",
                                "POST.PR"]) + "\n")
    for indv in indv_post_pr.keys():
        post_pr_dict = indv_post_pr[indv]
        tree_root.sum_post_prs(post_pr_dict)
        haplogroup = tree_root.get_highest_post_pr(post_pr_dict, args.min_post_pr)
        major_clade = re.findall("([A-Za-z]+)\d?", haplogroup.name)[0]
        
        sys.stdout.write("%s\t%s\t%s\t%g\n" % (indv, major_clade, haplogroup.name,
                                               post_pr_dict[haplogroup.name]))
    

main()
        

